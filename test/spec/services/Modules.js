'use strict';

describe('Service: Modules', function () {

  // load the service's module
  beforeEach(module('jpAppApp'));

  // instantiate service
  var Modules;
  beforeEach(inject(function (_Modules_) {
    Modules = _Modules_;
  }));

  it('should do something', function () {
    expect(!!Modules).toBe(true);
  });

});
