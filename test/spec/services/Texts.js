'use strict';

describe('Service: Texts', function () {

  // load the service's module
  beforeEach(module('jpAppApp'));

  // instantiate service
  var Texts;
  beforeEach(inject(function (_Texts_) {
    Texts = _Texts_;
  }));

  it('should do something', function () {
    expect(!!Texts).toBe(true);
  });

});
