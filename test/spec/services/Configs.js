'use strict';

describe('Service: Configs', function () {

  // load the service's module
  beforeEach(module('jpAppApp'));

  // instantiate service
  var Configs;
  beforeEach(inject(function (_Configs_) {
    Configs = _Configs_;
  }));

  it('should do something', function () {
    expect(!!Configs).toBe(true);
  });

});
