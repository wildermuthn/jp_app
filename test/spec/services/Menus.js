'use strict';

describe('Service: Menus', function () {

  // load the service's module
  beforeEach(module('jpAppApp'));

  // instantiate service
  var Menus;
  beforeEach(inject(function (_Menus_) {
    Menus = _Menus_;
  }));

  it('should do something', function () {
    expect(!!Menus).toBe(true);
  });

});
