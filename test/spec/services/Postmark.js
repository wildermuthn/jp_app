'use strict';

describe('Service: Postmark', function () {

  // load the service's module
  beforeEach(module('jpAppApp'));

  // instantiate service
  var Postmark;
  beforeEach(inject(function (_Postmark_) {
    Postmark = _Postmark_;
  }));

  it('should do something', function () {
    expect(!!Postmark).toBe(true);
  });

});
