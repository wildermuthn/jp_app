'use strict';

describe('Service: Trainings', function () {

  // load the service's module
  beforeEach(module('jpAppApp'));

  // instantiate service
  var Trainings;
  beforeEach(inject(function (_Trainings_) {
    Trainings = _Trainings_;
  }));

  it('should do something', function () {
    expect(!!Trainings).toBe(true);
  });

});
