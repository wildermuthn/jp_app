'use strict';

describe('Controller: ModulepassedCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var ModulepassedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModulepassedCtrl = $controller('ModulepassedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
