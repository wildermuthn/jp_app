'use strict';

describe('Controller: ManageAccountCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var ManageAccountCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManageAccountCtrl = $controller('ManageAccountCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
