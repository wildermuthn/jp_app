'use strict';

describe('Controller: AdminQuestionEditorCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var AdminQuestionEditorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminQuestionEditorCtrl = $controller('AdminQuestionEditorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
