'use strict';

describe('Controller: ModulePageCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var ModulePageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModulePageCtrl = $controller('ModulePageCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
