'use strict';

describe('Controller: UserExistsCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var UserExistsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserExistsCtrl = $controller('UserExistsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
