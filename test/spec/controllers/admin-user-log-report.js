'use strict';

describe('Controller: AdminUserLogReportCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var AdminUserLogReportCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminUserLogReportCtrl = $controller('AdminUserLogReportCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
