'use strict';

describe('Controller: ModulePassedCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var ModulePassedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModulePassedCtrl = $controller('ModulePassedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
