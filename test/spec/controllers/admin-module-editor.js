'use strict';

describe('Controller: AdminModuleEditorCtrl', function () {

  // load the controller's module
  beforeEach(module('jpAppApp'));

  var AdminModuleEditorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminModuleEditorCtrl = $controller('AdminModuleEditorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
