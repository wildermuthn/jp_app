'use strict';

describe('Directive: moduleIntroduction', function () {

  // load the directive's module
  beforeEach(module('jpAppApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<module-introduction></module-introduction>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the moduleIntroduction directive');
  }));
});
