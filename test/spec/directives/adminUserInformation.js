'use strict';

describe('Directive: adminUserInformation', function () {

  // load the directive's module
  beforeEach(module('jpAppApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<admin-user-information></admin-user-information>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the adminUserInformation directive');
  }));
});
