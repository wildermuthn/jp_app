'use strict';

describe('Directive: listQuestions', function () {

  // load the directive's module
  beforeEach(module('jpAppApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<list-questions></list-questions>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the listQuestions directive');
  }));
});
