'use strict';

angular.module('jpAppApp')
  .factory('Questions', function (Database) {
    // Service logic
    // ...
    var _db = {
      path: 'questions/',
      add: function(data, cb) {
        Database.add(this.path, null, data, function(result){
          cb(result);
        });
      },
      get: function(id, cb) {
        //console.log(id);
        Database.get(this.path + id, function(obj) {
          cb(obj);
        });
      },
      remove: function(id, cb) {
        Database.remove(this.path + id, cb);
      },
      index: function(cb) {
        Database.get(this.path, function(obj) {
          cb(obj);
        });
      },
      set: function(id, data, cb) {
        Database.set(this.path + id, data, cb);
      }
    };

    // Public API here
    return {
      add: function(data, cb) {
        _db.add(data, function(result){
          cb(result);
        });
      },
      remove: function(id, cb) {
        _db.remove(id, cb);
      },
      get: function(id, cb) {
        _db.get(id, cb);
      },
      set: function(id, data, cb) {
        _db.set(id, data, cb);
      }
    };
  });
