'use strict';

angular.module('jpAppApp')
  .factory('Users', function (Database, Auth, REST) {
    // Service logic
    // ...
    var _db = 'users/',
    _methods = {
      currentUser: {},
      resetPassword: function(userId, cb) {
        REST.resetPassword(userId, cb);
      },
      loadUser: function(userId, cb) {
        Database.get(_db + userId, function(userObject) {
          delete userObject.password;
          delete userObject.passwordConfirm;
          _methods.currentUser = userObject;
          cb(userObject);
        });
      },
      addNodeUser: function(data, cb) {
        REST.addUser(data, function(result) {
          //console.log(result);
          cb(result);
        });
      },
      getCurrentUser: function(cb) {
        cb(_methods.currentUser);
      },
      logSuccess: function(module, cb) {
        Auth.getUserId(function(userId) {
          _methods.loadUser(userId, function(userObj) {
            var userData = angular.copy(userObj);
            if (userData.modulesCompleted === undefined) {
              userData.modulesCompleted = {};
            }
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var time = curr_date + "-" + curr_month + "-" + curr_year;
            userData.modulesCompleted[module.id] = {
              name: module.title,
              time: time
            };
            Database.update(_db + userId, userData, function() {
              _methods.currentUser = userData;
              cb();
            });
          });
        });
      },
      login: function(userId, password, cb) {
        Auth.login(userId, password, function(result) {
          _methods.loadUser(result.id, function(){
            cb(result);
          });
        });
      },
      checkUserExists: function(userData, cb) {
        REST.checkUserExists(userData, cb);
      }
    };

    // Public API here
    return {
      index: function(cb) {
        Database.get(_db, cb);
      },
      logSuccess: function(module, cb) {
        _methods.logSuccess(module, cb);
      },
      addUser: function(userData, cb) {
        _methods.checkUserExists(userData, function(res) {
          //console.log('user exists: ' + res);
          var tempPassword = userData.password;
          if (res) {
            delete userData.password;
            delete userData.passwordConfirm;
            Database.add(_db, null, userData, function(result){
              userData.firebaseId = result;
              userData.password = tempPassword;
              _methods.addNodeUser(userData, function(resultB) {
                //console.log('Adduser CB: ' + resultB);
                //console.log((typeof resultB));
                cb(resultB);
              });
            });
          }
          else {
            cb(res);
          }
        });
      },
      removeUser: function(userId, cb) {
        Database.remove(_db + userId, cb);
      },
      loginUser: function(userId, password, cb) {
        _methods.login(userId, password, cb);
      },
      logoutUser: function(cb) {
        Auth.logout(cb);
      },
      resetPassword: function(userId, cb) {
        _methods.resetPassword(userId, cb);
      },
      updateUser: function(userId, userData, cb) {
        Database.update(_db + userId, userData, cb);
      },
      loadUser: function(userId, cb) {
        _methods.loadUser(userId, cb);
      },
      getCurrentUser: function(cb) {
        _methods.getCurrentUser(cb);
      }
    };
  });
