'use strict';

angular.module('jpAppApp')
  .factory('Texts', function (Database, $location) {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      globalGet: function(id, cb) {
        Database.get('texts/' + id, function(text) {
          if (text !== undefined && text !== false) {
            cb(text);
          }
          else {
            cb(false);
          }
        });
      },
      set: function(data, cb) {
        Database.set('texts', data, function(response) {
          cb(response);
        });
      },
      index: function(cb) {
        Database.get('texts', function(text) {
          cb(text);
        });
      },
      get: function(cb) {
        Database.get('texts' + $location.path(), function(text) {
          if (text !== undefined && text !== false) {
            cb(text);
          }
          else {
            cb(false);
          }
        });
      }
    };
  });
