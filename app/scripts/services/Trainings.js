'use strict';

angular.module('jpAppApp')
  .factory('Trainings', function (Database, Modules) {
    // Service logic
    // ...
    var _db = {
      path: 'trainings/',
      add: function(data, cb) {
        Database.add(this.path, null, data, function(result){
          cb(result);
        });
      },
      load: function(userId, cb) {
        Database.get(this.path + userId, function(trainingObject) {
          cb(trainingObject);
        });
      },
      remove: function(trainingId, cb) {
        Database.remove(this.path + trainingId, cb);
      },
      index: function(cb) {
        Database.get(this.path, function(indexObject) {
          cb(indexObject);
        });
      },
      set: function(id, data, cb) {
        Database.set(this.path + id, data, cb);
      }
    };

    // Public API here
    return {
      add: function(trainingData, cb) {
        _db.add(trainingData, function(result){
          cb(result);
        });
      },
      remove: function(trainingId, cb) {
        _db.remove(trainingId, cb);
      },
      load: function(trainingId, cb) {
        _db.load(trainingId, cb);
      },
      index: function(cb) {
        _db.index(cb);
      },
      initializeTest: function(trainingId, cb) {
        Modules.add({test: true, title: "Test Module"}, cb);
      },
      set: function(id, data, cb) {
        _db.set(id, data, cb);
      }
    };
  });
