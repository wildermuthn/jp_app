'use strict';

angular.module('jpAppApp')
  .factory('Email', function (REST) {
    // Service logic
    // ...
    var _email = {
      send: function(data, cb) {
        REST.sendEmail(data, cb);
      }
    }

    // Public API here
    return {
      send: function (data, cb) {
        _email.send(data, cb);
      }
    };
  });
