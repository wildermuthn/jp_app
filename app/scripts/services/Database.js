'use strict';

angular.module('jpAppApp')
.factory('Database', function (Auth, Firebase, $log) {
  // Service logic
  // ...
  function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
  }


  var _db = {
    database: Firebase,
    call: function(fn, args) {
      
      NProgress.start();
      function insertFunction(args, v) {
        return function(result) {
          NProgress.done();
          args[v].call(null, result);
        };
      }
      var newCb = null;
      var newArgs = [];
      for (var v in args) {
        if (isFunction(args[v])) {
          newCb = insertFunction(args, v);
          newArgs.push(newCb);
        }
        else {
          newArgs.push(args[v]);
        }
      }
      this.database[fn].apply(this.database, newArgs);
    }
  };

  // Public API here
  return {
    get: function(id, cb) {
      _db.call('get', [id, cb]);
    },
    getStream: function(id, cb) {
      _db.call('getStream', [id, cb]);
    },
    // updating is bool
    set: function(id, value, cb) {
      _db.call('set', [id, value, cb]);
    },
    update: function(id, value, cb) {
      _db.call('update', [id, value, cb]);
    },
    remove: function(id, cb) {
      _db.call('remove', [id, cb]);
    },
    // setId is optional string 
    add: function(parentId, setId, value, cb) {
      _db.call('add', [parentId, setId, value, cb]);
    },
  };
});
