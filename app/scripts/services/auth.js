'use strict';

angular.module('jpAppApp')
  .factory('Auth', function (REST, Firebase, LocalStorage) {
    // Service logic
    // ...
    var _user = {
      authObject: null,
      authenticated: false,
      login: function(userId, password, cb) {
        REST.login(userId, password, function(token) {
          if (token) {
            _user.firebaseAuthorize(token, cb);
          }
          else {
            cb(false);
          }
        });
      },
      firebaseAuthorize: function(token, cb) {
          Firebase.authorize(token, function(result) {
            if (result) {
              _user.authObject = result;
              _user.saveToken(token);
              _user.authenticated = true;
              cb(result);
            }
            else {
              cb(false);
            }
          });
      },
      logout: function(cb) {
        this.clearToken();
        this.authenticated = false;
        this.authObject = null;
        cb();
      },
      getToken: function() {
        return LocalStorage.get('token');
      },
      saveToken: function(token) {
        LocalStorage.set('token', token);
      },
      clearToken: function() {
        LocalStorage.remove('token');
      },
      checkAuthentication: function(cb) {
        if (this.authenticated === true) {
          cb(_user.authObject);
        }
        else {
          if (this.getToken() !== undefined && this.getToken() !== null) {
            this.firebaseAuthorize(this.getToken(), function(result) {
              cb(result);
            });
          }
          else {
            cb(false);
          }
        }
      },
      getUserId: function(cb) {
        if (this.getToken() !== undefined && this.getToken() !== null) {
          Firebase.authorize(this.getToken(), function(result) {
            if (result) {
              cb(result.auth.id);
            }
            else {
              cb(false);
            }
          });
        }
        else {
          cb(false);
        }
      }
    };

    // Public API here
    return {
      isAuthenticated: function(cb) {
        _user.checkAuthentication(cb);
      },
      login: function(userId, password, cb) {
        _user.login(userId, password, cb);
      },
      logout: function(userId, cb) {
        _user.logout(userId, cb);
      },
      getAuth: function() {
        return _user.authObject;
      },
      getUserId: function(cb) {
        _user.getUserId(cb);
      }
    };
      
  });
