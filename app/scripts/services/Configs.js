'use strict';

angular.module('jpAppApp')
  .factory('Configs', function ($rootScope) {
    // Service logic
    // ...
    var _configs = {
      firebase: {
        dbName: '501-training-applications',
        dbRoot: 'jp',
      },
      restServer: {
        url: 'jptrainingcenter.org',
        path: '/rest'
      }
    };
    NProgress.configure({ speed: 100, trickleRate: 0.1, trickleSpeed: 100 });
    $rootScope.globals = {};
    $rootScope.globals.wEditor = {
	    options : {
		    theme: "modern",
		    menubar: false,
		    plugins: [
		            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
			    "searchreplace wordcount visualblocks visualchars code fullscreen",
			    "insertdatetime media nonbreaking save table contextmenu directionality",
			    "emoticons template paste textcolor",
                            "moxiemanager",
			],
		    toolbar_items_size: 'small',
		    toolbar1: "code insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
		    image_advtab: true,
		    width: "98%",
		    autoresize_max_height: '500'
		}
    }
    // Public API here
    return _configs;
  });
