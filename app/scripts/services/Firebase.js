'use strict';

angular.module('jpAppApp')
  .factory('Firebase', function (Configs) {
    // Service logic
    // ...
    var _db = new Firebase('https://' + Configs.firebase.dbName + '.firebaseio.com/' + Configs.firebase.dbRoot + '/');
    var _methods = {
      authorize: function(token, cb) {
        var temp = _db.auth(token, function(error, result){
          cb(result);
        });
      },
      valueOn: function(id, cb) {
        _db.child(id).on('value', function(ss) {
          var name = ss.name();
          var value = (ss.val() != null) ? ss.val() : false;
          value.firebaseId = name;
          cb(value);
        });
      },
      valueOnce: function(id, cb) {
        _db.child(id).once('value', function(ss) {
          var name = ss.name();
          var tempValue = null;
          var value = (ss.val() != null) ? ss.val() : false;
          tempValue = angular.copy(value);
          tempValue['firebaseId'] = name;
          cb(tempValue);
        });
      },
      valueUpdate: function(id, value, cb) {
        delete value.firebaseId;
        _db.child(id).update(value, cb);
      },
      valueSet: function(id, value, cb) {
        delete value.firebaseId;
        var nohashValue = angular.copy(value);
        _db.child(id).set(nohashValue, cb);
      },
      remove: function(id, cb) {
        var ref = _db.child(id);
        ref.remove(cb);
      },
      add: function(parentId, setId, value, cb) {
        var nohashValue = angular.copy(value);
        if (setId !== null) {
          if (parentId !== null && parentId !== '' ) {
            _db.child(parentId + '/' + setId).set(nohashValue, cb);
          }
        }
        else {
          var ref = _db.child(parentId).push(nohashValue);
          cb(ref.name());
        }
      }
    };
    // Public API here
    return {
      authorize: function(token, cb) {
        _methods.authorize(token, cb);
      },
      // streaming is bool
      get: function(id, cb) {
          _methods.valueOnce(id, cb);
        },
      getStream: function(id, cb) {
          _methods.valueOn(id, cb);
        },
      // updating is bool
      set: function(id, value, cb) {
          _methods.valueSet(id, value, cb);
        },
      update: function(id, value, cb) {
          _methods.valueUpdate(id, value, cb);
        },
      remove: function(id, cb) {
          _methods.remove(id, cb);
        },
      // setId is optional string 
      add: function(parentId, setId, value, cb) {
          _methods.add(parentId, setId, value, cb);
        },
    };
  });
