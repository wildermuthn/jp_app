'use strict';

angular.module('jpAppApp')
  .factory('REST', function ($http, Configs) {
    // Service logic
    // ...
    var _rest = {
      //endpoint: 'http://' + Configs.restServer.url + Configs.restServer.path + '/',
      endpoint: 'http://local.justine/rest/',
      login: function(userId, password, cb) {
        $http({
          method: 'POST',
          data: {
            userId: userId,
            password: password
          },
          url: this.endpoint  + 'login'
        }).
        success(function(token) {
          if (token !== 'false') {
            cb(JSON.parse(token));
          }
          else {
            cb(false);
          }
        });
      },
      resetPassword: function(userId, cb) {
        $http({
          method: 'POST',
          data: {
            userId: userId,
          },
          url: this.endpoint  + 'password-reset'
        }).
        success(function(result) {
          cb(result);
        });
      },
      sendEmail: function(data, cb) {
        $http({
          method: 'POST',
          data: data,
          url: this.endpoint + 'send-email'
        }).
        success(function(result) {
          cb(result);
        });
      },
      addUser: function(userData, cb) {
        $http({
          method: 'POST',
          data: {
            userId: userData.emailAddress,
            password: userData.password,
            id: userData.firebaseId
          },
          url: this.endpoint  + 'register'
        }).
        success(function(result) {
          cb(JSON.parse(result)); });
      },
      checkUserExists: function(userData, cb) {
        $http({
          method: 'POST',
          data: {
            userId: userData.emailAddress,
            password: userData.password,
            id: userData.firebaseId
          },
          url: this.endpoint  + 'check-user-exists'
        }).
        success(function(result) {
          cb(JSON.parse(result)); });
      }
    };

    // Public API here
    return {
      login: function(userId, password, cb) {
        _rest.login(userId, password, cb);
      },
      checkUserExists: function(userData, cb) {
        _rest.checkUserExists(userData, cb);
      },
      resetPassword: function(userId, cb) {
        _rest.resetPassword(userId, cb);
      },
      addUser: function(userData, cb) {
        _rest.addUser(userData, cb);
      },
      sendEmail: function(data, cb) {
        _rest.sendEmail(data, cb);
      }
    };
  });
