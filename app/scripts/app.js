'use strict';

angular.module('jpAppApp', ['ngRoute', 'ui.tinymce', 'ngAnimate', 'ui.sortable'])
.config(function ($routeProvider, $logProvider) {
  $routeProvider
  .when('/', {
    redirectTo: '/home'
  })
  .when('/home', {
    templateUrl: 'views/home.html',
    controller: 'HomeCtrl'
  })
  .when('/about', {
    templateUrl: 'views/home.html',
    controller: 'HomeCtrl'
  })
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl'
  })
  .when('/logout', {
    templateUrl: 'views/home.html',
    controller: 'HomeCtrl'
  })
  .when('/password-reset', {
    templateUrl: 'views/password-reset.html',
    controller: 'PasswordResetCtrl'
  })
  .when('/create-account', {
    templateUrl: 'views/create-account.html',
    controller: 'CreateAccountCtrl'
  })
  .when('/manage-account', {
    templateUrl: 'views/manage-account.html',
    controller: 'ManageAccountCtrl'
  })
  .when('/user-exists', {
    templateUrl: 'views/user-exists.html',
    controller: 'UserExistsCtrl'
  })
  .when('/trainings-index', {
    templateUrl: 'views/trainings-index.html',
    controller: 'TrainingsIndexCtrl'
  })
  .when('/modules-index', {
    templateUrl: 'views/modules-index.html',
    controller: 'ModulesIndexCtrl'
  })
  .when('/module/:moduleId', {
    templateUrl: 'views/module-page.html',
    controller: 'ModulePageCtrl',
    resolve: {
      module: ['$q', '$timeout', 'Modules', 'Questions', '$route', function($q, $timeout, Modules, Questions, $route) {
        var defer = $q.defer();
        Modules.get($route.current.params.moduleId, function(result) {
          var questions = [];
          _.each(result.questions, function(question, questionKey) {
            var aDefer = $q.defer();
            questions.push(aDefer.promise);
            Questions.get(question.id, function(qresult) {
              result['questions'][questionKey].data = angular.copy(qresult);
              aDefer.resolve();
            });
          });
          var questionsPromises = $q.all(questions);
          questionsPromises.then(function() {
            defer.resolve(result);
          });
        });
        return defer.promise;
      }]
    }
  })
  .when('/module/:moduleId/passed', {
    templateUrl: 'views/module-passed.html',
    controller: 'ModulePassedCtrl',
    resolve: {
      module: ['$q', '$timeout', 'Modules', 'Questions', '$route', function($q, $timeout, Modules, Questions, $route) {
        var defer = $q.defer();
        Modules.get($route.current.params.moduleId, function(result) {
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .when('/module/:moduleId/notify', {
    templateUrl: 'views/module-complete-notification.html',
    controller: 'ModuleCompleteNotificationCtrl',
    resolve: {
      module:  ['$q', '$timeout', 'Modules', 'Questions', '$route', function($q, $timeout, Modules, Questions, $route) {
        var defer = $q.defer();
        Modules.get($route.current.params.moduleId, function(result) {
            result.id = $route.current.params.moduleId;
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .when('/admin-home', {
    templateUrl: 'views/admin-home.html',
    controller: 'AdminHomeCtrl',
    resolve: {
      texts: ['$q', '$timeout', 'Texts', function($q, $timeout, Texts) {
        var defer = $q.defer();
        Texts.index(function(result) {
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .when('/admin/users', {
    templateUrl: 'views/admin-manage-users.html',
    controller: 'AdminManageUsersCtrl',
    resolve: {
      users: ['$q', '$timeout', 'Users', function($q, $timeout, Users) {
        var defer = $q.defer();
        Users.index(function(result) {
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .when('/admin/user/:userId', {
    templateUrl: 'views/admin-user-log-report.html',
    controller: 'AdminUserLogReportCtrl',
    resolve: {
      user: ['$q', '$timeout', 'Users', '$route', function($q, $timeout, Users, $route) {
        var defer = $q.defer();
        Users.loadUser($route.current.params.userId, function(result) {
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .when('/my-account', {
    templateUrl: 'views/admin-user-log-report.html',
    controller: 'AdminUserLogReportCtrl',
    resolve: {
      user: ['$q', '$timeout', 'Users', '$route', 'Auth', '$location', function($q, $timeout, Users, $route, Auth, $location) {
        var defer = $q.defer();
        Auth.isAuthenticated(function(result) {
          if (result !== false) {
            Users.loadUser(result.auth.id, function(result) {
                defer.resolve(result);
            });
          }
          else {
            alertify.error('Please login first.');
            $location.path('home');
          } 
        });
        return defer.promise;
      }]
    }
  })
  .when('/admin-training/:trainingId', {
    templateUrl: 'views/admin-training-editor.html',
    controller: 'AdminTrainingEditorCtrl',
    resolve: {
      training: ['$q', '$timeout', 'Trainings', '$route', function($q, $timeout, Trainings, $route) {
        var defer = $q.defer();
        Trainings.load($route.current.params.trainingId, function(result) {
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .when('/admin-module/:moduleId', {
    templateUrl: 'views/admin-module-editor.html',
    controller: 'AdminModuleEditorCtrl',
    resolve: {
      module: ['$q', '$timeout', 'Modules', 'Questions', '$route', function($q, $timeout, Modules, Questions, $route) {
        var defer = $q.defer();
        Modules.get($route.current.params.moduleId, function(result) {
          var questions = [];
          _.each(result.questions, function(question, questionKey) {
            var aDefer = $q.defer();
            questions.push(aDefer.promise);
            Questions.get(question.id, function(qresult) {
              result['questions'][questionKey].data = angular.copy(qresult);
              aDefer.resolve();
            });
          });
          var questionsPromises = $q.all(questions);
          questionsPromises.then(function() {
            defer.resolve(result);
          });
        });
        return defer.promise;
      }]
    }
  })
  .when('/admin-question/:questionId', {
    templateUrl: 'views/admin-question-editor.html',
    controller: 'AdminQuestionEditorCtrl',
    resolve: {
      question: ['$q', '$timeout', 'Questions', '$route', function($q, $timeout, Questions, $route) {
        var defer = $q.defer();
        Questions.get($route.current.params.questionId, function(result) {
            defer.resolve(result);
        });
        return defer.promise;
      }]
    }
  })
  .otherwise({
    redirectTo: '/'
  });
});
