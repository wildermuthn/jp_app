'use strict';

angular.module('jpAppApp')
  .directive('leftColumn', function () {
    return {
      scope: '&',
      templateUrl: 'views/left-column.html',
      transclude: true,
      restrict: 'A',
      replace: true,
      link: function postLink(scope, element, attrs) {
      }
    };
  });
