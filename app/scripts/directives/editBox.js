'use strict';

angular.module('jpAppApp')
  .directive('editBox', function ($rootScope) {
    return {
      scope: {
        model: '=',
        header: '@',
        options: '=',
        },
      templateUrl: 'views/edit-box-tpl.html',
      replace: true,
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
      }
    };
  });
