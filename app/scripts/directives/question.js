'use strict';

angular.module('jpAppApp')
  .directive('question', function (Questions) {
    return {
      replace: true,
      scope: {
     	question: '=',
	editor: '&'
      },
      templateUrl: 'views/questions.html',
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
       //console.log(scope.question);
       scope.validateAnswer = function(answer) {
        scope.answerVisible = answer;
	//console.log(answer.check);
	scope.question.correct = answer.check;
	var sendObj = {
	  id: scope.question.firebaseId,
	  result: scope.question.correct,
	};
	scope.$emit('questionAnswered', sendObj);
       }
    }
    };
  });
