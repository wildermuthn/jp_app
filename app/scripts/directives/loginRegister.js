'use strict';

angular.module('jpAppApp')
  .directive('loginRegister', function () {
    return {
      scope: {
        submit: '&'
      },
      templateUrl: 'views/login-register-tpl.html',
      replace: true,
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        //console.log(attrs.submit);
      }
    };
  });
