'use strict';

angular.module('jpAppApp')
  .directive('userForm', function () {
    return {
      scope: {
        submit: '&'
      },
      templateUrl: 'views/user-form.html',
      replace: true,
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        //console.log(attrs.submit);
      }
    };
  });
