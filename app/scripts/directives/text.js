'use strict';

angular.module('jpAppApp')
  .directive('text', function (Texts, $sce) {
    return {
      scope: {},
      replace: true,
      templateUrl: 'views/text.html',
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        var displayText = function(text, global) {
          if (text && global == null) {
            scope.$apply(function() {
              scope.header = text[attrs.text].header;
              scope.body = $sce.trustAsHtml(text[attrs.text].body);
            });
          }
          else if (global == null){
            scope.$apply(function() {
              scope.header = 'No Header';
              scope.body = $sce.trustAsHtml('No Body');
            });
          }
          else {
            scope.$apply(function() {
              scope.header = text.header;
              scope.body = $sce.trustAsHtml(text.body);
            });
          }
        }
        if (attrs.global == null) {
          scope.type = "hero-unit";
          Texts.get(function(text) {
            displayText(text);
          });
        }
        else {
          Texts.globalGet(attrs.global, function(text) {
            displayText(text, true);
          });
        }
      }
    };
  });
