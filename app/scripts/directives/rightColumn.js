'use strict';

angular.module('jpAppApp')
  .directive('rightColumn', function () {
    return {
      templateUrl: 'views/right-column.html',
      transclude: true,
      restrict: 'A',
      replace: true,
      link: function postLink(scope, element, attrs) {
      }
    };
  });
