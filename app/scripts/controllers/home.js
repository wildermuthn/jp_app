'use strict';

angular.module('jpAppApp')
  .controller('HomeCtrl', function ($scope, $log, $timeout, $rootScope, Auth) {
    Auth.isAuthenticated(function(result) {
      ////console.log(result);
      if (result) {
        $scope.loggedIn = true;
        if (result.auth.role === 'admin') {
          $scope.isAdmin = true;
        }
      }
      else {
        $scope.loggedIn = false;
      }
    });
  	$scope.homeCtrl = {};
  });
