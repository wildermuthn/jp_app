'use strict';

angular.module('jpAppApp')
  .controller('AdminTrainingEditorCtrl', function ($scope, training, Modules, Trainings) {
    if (training.test != undefined) {
      $scope.test = training.test;
    }
    else {
      Trainings.initializeTest(training.firebaseId, function(id) {
        console.log(id);
        $scope.test = id;
        training.test = id;
        Trainings.set(training.firebaseId, angular.copy(training), function() {
        });
      });
    }
    $scope.rows = [];
    $scope.setRows = function(modules, cb) {
      $scope.rows = [];
      var count = 0;
      _.each(modules, function(module, key) {
        if (module != 'modules') {
          module.id = key;
          $scope.rows.push(module);
        }
      });
      ////console.log($scope.rows);
      cb();
    }
    $scope.resetOrder = function (cb) {
      var order = 0;
      $('#rows tr').each(function(){
        ////console.log($(this).attr('id'));
        var id = $(this).attr('id');
        _.each($scope.rows, function(row) {
          if (row.id == id) {
            row.order = order;
            $scope.training.modules[row.id] = row; 
          }
        });
        order++;
      });
      $scope.save();
      cb();
    }
    $scope.sortOptions = {
        update: function(e, ui) { 
          $scope.resetOrder(function() {
            alertify.success('Order saved');
          });
        },
    };
    var _changeTitle = function(moduleId, id) {
      Modules.get(moduleId, function(result) {
        $scope.$apply(function() {
          if (result === false) {
            delete training.modules[id];
          }
          else {
            training.modules[id].title = result.title;
          }
        });
      });
    };
    $scope.training = training;
    $scope.setRows(training.modules, function() {});
    ////console.log(training);
    _.each(training.modules, function(module, id) {
      var moduleId = module.id;
      _changeTitle(moduleId, id);
    });
    
    $scope.fixObj = function(obj) {
      var temp = _.toArray(obj);
      return temp;
    };
    $scope.deleteModule = function(id, title) {
      ////console.log('Delete ' + id);
      alertify.confirm('Are you sure you want to delete "' + title + '"?', function(result) {
        if (result) {
          ////console.log('Delete ' + id);
          Modules.remove(id, function() {
            $scope.$apply(function() {
              _.each($scope.training.modules, function(module, moduleId) {
                if (module.id === id) {
                  delete $scope.training.modules[moduleId];
                }
              });
              $scope.setRows($scope.training.modules, function() {
                $scope.resetOrder(function(){
                  $scope.save();
                });
              });
            });
          });
        }
        else {
        }
      });
    };
    $scope.toggleModuleStatus = function(id) {
      ////console.log(id);
      ////console.log($scope.training);
      _.each($scope.training.modules, function(module) {
        if (module.id == id) {
          if (module.published === 'false' || module.published == undefined) {
            module.published = 'true';
          }
          else {
            module.published = 'false';
          }
        }
      });
      $scope.save();
    };
    $scope.save = function() {
      Trainings.set($scope.training.firebaseId, angular.copy($scope.training), function() {
        alertify.success('Saved');
      });
    };
    $scope.addModule = function () {
      alertify.prompt("What is the name of the new module?", function (e, str) {
        if (e) {
          Modules.add({
            title: str
          }, function(firebaseId) {
            $scope.$apply(function(){
              if ($scope.training.modules == undefined) {
                $scope.training.modules = {};
              }
              $scope.training.modules[firebaseId] = {
                title: str,
                id: firebaseId,
                order: $scope.rows.length
              };
              $scope.setRows($scope.training.modules, function() {
                  $scope.save();
              });
            });
          });
        }
        else {
        }
      }, "A Module");
    };
      
  });
