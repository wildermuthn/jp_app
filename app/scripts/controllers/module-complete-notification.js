'use strict';

angular.module('jpAppApp')
  .controller('ModuleCompleteNotificationCtrl', function ($scope, Email, module, Users, Auth, Trainings) {
    $scope.emailSent = false;
    Users.logSuccess(module, function() {
    });
    
    $scope.print = function() {
      window.print();
    };

    $scope.sendEmail = function(email) {
      Users.getCurrentUser(function(user) {
        //console.log(user);
        var from = user.emailAddress;
        Email.send({
          user: user,
          to: email,
          from: from,
          moduleTitle: module.title,
        }, 
        function(result){
          $scope.emailSent = true;
          if (result) {
            alertify.success('Email Sent!');
          }
          else {
            alertify.error('Something went wrong. Try again.')
          }
        });
      });
    }
    //console.log('Notify someone');
  });
