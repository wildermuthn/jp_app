'use strict';

angular.module('jpAppApp')
  .controller('AdminManageUsersCtrl', function ($scope, users, $location, Users) {
    $scope.users = users;
    $scope.location = $location;
    //console.log(users);
    $scope.editUser = function(id) {
      //console.log(id);
      $location.path('admin/user/' + id);
    };
    $scope.deleteUser = function(id, name) {
      alertify.confirm('Are you sure you want to user "' + name + '"?', function(response) {
        if (response) {
          Users.removeUser(id, function() {
            Users.index(function(users) {
              $scope.$apply(function() {
                $scope.users = users;
              });
            });
            alertify.success('User has been removed');
          });
        }
        else {}
      });
    };
    $scope.resetPassword = function(id) {
      alertify.confirm('Are you sure you want reset the user with the email address of "' + id + '"?', function(response) {
        if (response) {
          Users.resetPassword(id, function() {
            alertify.success('The user\'s password has been reset and an email has been sent to the user with their password');
          });
        }
        else {}
      });
    };
  });
