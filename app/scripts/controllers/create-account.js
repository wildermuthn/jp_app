'use strict';

angular.module('jpAppApp')
  .controller('CreateAccountCtrl', function (Users, $scope, $location) {
    $scope.createAccount = function(data) {
      //console.log(data);
      Users.addUser(data, function(result) {
        //console.log('result: ' + result);
        if (result) {
          alertify.success('Account created successfully.');
          $location.path('/login');
        }
        else {
          alertify.error('Account creation failed. Try a different username and password.');
        }
      });
    };
  });
