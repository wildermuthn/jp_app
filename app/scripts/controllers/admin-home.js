'use strict';

angular.module('jpAppApp')
  .controller('AdminHomeCtrl', function ($scope, Trainings, Texts, $timeout, texts) {
    $scope.rows = [];
    $scope.setRows = function(trainings, cb) {
      $scope.rows = [];
      var count = 0;
      _.each(trainings, function(training, key) {
        if (training != 'trainings') {
          training.id = key;
          $scope.rows.push(training);
        }
      });
      ////console.log($scope.rows);
      cb();
    }
    $scope.resetOrder = function (cb) {
      var order = 0;
      $('#rows tr').each(function(){
        ////console.log($(this).attr('id'));
        var id = $(this).attr('id');
        _.each($scope.rows, function(row) {
          if (row.id == id) {
            row.order = order;
            Trainings.set(id, row, function() {
            });
          }
        });
        order++;
      });
      cb();
    }
    $scope.sortOptions = {
        update: function(e, ui) { 
          $scope.resetOrder(function() {
            alertify.success('Order saved');
          });
        },
    };
    Trainings.index(function(result) {
      ////console.log(result);
      $scope.setRows(result, function(){});
    });
    ////console.log(texts);
    $scope.textEdit = {};
    $scope.boxes = {};
    delete texts.firebaseId;
    _.each(texts, function(text, firebaseId) {
      if(firebaseId == 'home') {
        $scope.textEdit[firebaseId] = text['hero-unit'].body;
      }
      if (text.header == undefined) {
        _.each(text, function(box, boxId) {
          ////console.log(box);
          box.header = box.header + ' (/' + firebaseId + ')';
          $scope.boxes[firebaseId + '***' + boxId] = box;
        });
      }
      else {
        ////console.log('undefined header');
        ////console.log(text);
        $scope.boxes[firebaseId + '***global'] = text;
        //$scope.boxes[firebaseId] = text;
      }
    });
    $scope.fixObj = function(obj) {
      var temp = _.toArray(obj);
      return temp;
    };
    $scope.deleteTraining = function(id, title) {
      alertify.confirm('Are you sure you want to delete "' + title + '"?', function(result) {
        if (result) {
          ////console.log('Delete ' + id);
          Trainings.remove(id, function() {
              Trainings.index(function(result) {
                $scope.$apply(function() {
                  $scope.setRows(result, function() {
                    $scope.resetOrder(function(){
                      ////console.log(result);
                    });
                  });
                });
              });
          });
        }
        else {
        }
      });
    };
    $scope.toggleTrainingStatus = function(id, training) {
      if (training.published === 'false' || training.published == undefined) {
        training.published = 'true';
      }
      else {
        training.published = 'false';
      }
      Trainings.set(id, training, function() {
        alertify.success('Training status set');
      });
    };
    $scope.save = function() {
      var texts = {};
      var boxes = angular.copy($scope.boxes);
      _.each(boxes, function(box, key) {
        var parentKey = key.split('***')[0];
        var childKey = key.split('***')[1];
        box.header = box.header.split(' (/')[0];
        if (texts[parentKey] == undefined) {
          texts[parentKey] = {};
        }
        if (childKey == 'global') {
          texts[parentKey] = box;
        }
        else {
          texts[parentKey][childKey] = box;
        }
      });
      Texts.set(texts, function(){
        alertify.success('Saved'); 
      });
      ////console.log(texts);
    };
    $scope.addTraining = function () {
      alertify.prompt("What is the name of the new training?", function (e, str) {
        if (e) {
          Trainings.add({
            title: str,
            order: $scope.rows.length,
            modules: [],
          }, function(firebaseId) {
            $scope.$apply(function(){
              $scope.rows.push({
                title: str,
                id: firebaseId,
                order: $scope.rows.length
              });
              alertify.success('Saved');
            });
          });
        }
        else {
        }
      }, "A Training");
    };
  });
