'use strict';

angular.module('jpAppApp')
  .controller('LoginCtrl', function ($scope, Users, $location, Auth) {
    $scope.login = function(data) {
      ////console.log(data);
      if (data != undefined) {
        Users.loginUser(data.emailAddress, data.password, function(result) {
          if (result) {
            alertify.success('Successful Login');
            $scope.$apply(function(){
              $location.path('/trainings-index');
            });
          }
          else {
            alertify.error('Login failed');
          }
          ////console.log(result);
        });
      }
    }
  });
