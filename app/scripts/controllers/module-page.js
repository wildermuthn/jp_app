'use strict';

angular.module('jpAppApp')
  .controller('ModulePageCtrl', function ($scope, Questions, $routeParams, module, $sce, $timeout, $location) {
    module.questions = _.toArray(module.questions);
    var questionTracker = [];
    _.each(module.questions, function(question, key) {
      //console.log(question);
      if (question.published == 'false' || question.published == undefined) {
        delete module.questions[key];  
      }
    });
    $scope.module = module;
    module.trainingText = $sce.trustAsHtml(module.trainingText);
    module.epilogue = $sce.trustAsHtml(module.epilogue);
    _.each(module.questions, function(question, questionKey) {
      //console.log(question);
      questionTracker.push(question.id);
      module.questions[questionKey].data.body = $sce.trustAsHtml(module.questions[questionKey].data.body);
      module.questions[questionKey].data.wrongmessage = $sce.trustAsHtml(module.questions[questionKey].data.wrongmessage);
      module.questions[questionKey].data.correctmessage = $sce.trustAsHtml(module.questions[questionKey].data.correctmessage);
    });
    //console.log($scope.module.questions);
    $scope.$on('questionAnswered', function(e, data) {
        //console.log('question answered received');
    	//console.log(data);
        if (data.result === true) {
          //console.log('removing answer');
          questionTracker = _.without(questionTracker, data.id);
          //console.log(questionTracker);
        }
        //console.log(questionTracker.length);
        if (questionTracker.length === 0) {
          alertify.success('You have completed all the answers correctly!');
          $timeout(function(){
            $scope.passed = true;
          },0);
        }
    });
    $scope.passTest = function() {
      $location.path('module/' + $routeParams.moduleId + '/passed');
    }
  });
