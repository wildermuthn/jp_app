'use strict';

angular.module('jpAppApp')
  .controller('AdminQuestionEditorCtrl', function ($scope, question, Questions) {
    $scope.answers = {};
    $scope.question = question;
    if ($scope.question.answers === undefined) {
      $scope.question.answers = [];
    }
    ////console.log(question);
    $scope.fixObj = function(obj) {
      var temp = _.toArray(obj);
      return temp;
    };
    $scope.save = function() {
      ////console.log($scope.question);
      Questions.set($scope.question.firebaseId, angular.copy($scope.question), function(result) {
        ////console.log(result);
        alertify.success('Saved');
      });
    };
    $scope.addAnswer = function() {
      $scope.question.answers.push('New Answer');
    };
    $scope.delete = function(index) {
      $scope.question.answers.splice(index, 1);
    };
  });
