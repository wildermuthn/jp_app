'use strict';

angular.module('jpAppApp')
  .controller('TrainingsIndexCtrl', function ($scope, Trainings, Auth, $rootScope, Users) {
    $scope._ = _;
    $scope.completedModules;
    Auth.isAuthenticated(function(result) {
      if (result.auth.role === 'admin') {
        $scope.isAdmin = true;
      }
      else {
        $scope.isAdmin = false;
      }
      Users.loadUser(result.auth.id, function(user) {
        $scope.$apply(function() {
          if (user.modulesCompleted != undefined) {
            $scope.completedModules = _.keys(user.modulesCompleted);
          }
          else {
            $scope.completedModules = [];
          }
        });
      });
    });
    Trainings.index(function(result) {
      delete result.firebaseId;
      $scope.$apply(function() {
        $scope.trainings = $rootScope.objToArray(result);
      });
    });
    $scope.fixObj = function(obj) {
      var temp = _.toArray(obj);
      return temp;
    };
     
  });

