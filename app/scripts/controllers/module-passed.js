'use strict';

angular.module('jpAppApp')
  .controller('ModulePassedCtrl', function ($scope, Questions, $routeParams, module, $sce, $timeout, $location) {
    $scope.module = module;
    module.epilogue = $sce.trustAsHtml(module.epilogue);
    $scope.finish = function() {
      $location.path('module/' + $routeParams.moduleId + '/notify');
    };
  });
