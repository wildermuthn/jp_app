'use strict';

angular.module('jpAppApp')
.controller('AppCtrl', function ($rootScope, Configs, Auth, $location, $window) {
  $rootScope.$window = $window;
  $rootScope.$location = $location;
  ////console.log($location.path());
  $rootScope.$on('$routeChangeStart', function() {
    NProgress.start();
  });
  $rootScope.$on('$routeChangeSuccess', function(next, current) {
    var path = (current.$$route !== undefined) ? current.$$route.originalPath : '404';
    Auth.isAuthenticated(function(result) {
      if (result) {
        if (path === '/logout') {
          Auth.logout(function() {
            $location.path('home');
          });
        }
        if (path === '/admin-home' && result.auth.role != 'admin') {
          alertify.error('You must be an administrator to view that page');
          $location.path('trainings-index');
        }
      }
      else {
        switch(path) {
        case '':
        case '/':
        case '/home':
        case '/about':
        case '/create-account':
        case '/login':
        case '/password-reset':
          break;
        default:
          alertify.error('You do not have permission to view that page');
          $location.path('login');
          break;
        }
      }
      NProgress.done();
    });
  });
  $rootScope.$on('$routeChangeError', function() {

  });
  $rootScope.objToArray = function(obj) {
    _.each(obj, function(item, key) {
      item.id = key;
    });
    var temp = _.toArray(obj);
    return temp;
  };
});
