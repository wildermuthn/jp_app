'use strict';

angular.module('jpAppApp')
  .controller('AdminModuleEditorCtrl', function ($scope, module, Questions, Modules) {
    ////console.log(module);
    $scope.rows = [];
    $scope.setRows = function(questions, cb) {
      $scope.rows = [];
      _.each(questions, function(question, key) {
        ////console.log(question);
        if (module !== 'questions') {
          question.id = key;
          $scope.rows.push(question);
        }
      });
      if (cb !== undefined) {
        cb();
      }
    };
    $scope.resetOrder = function (cb) {
      var order = 0;
      $('#rows tr').each(function(){
        ////console.log($(this).attr('id'));
        var id = $(this).attr('id');
        _.each($scope.rows, function(row) {
          if (row.id === id) {
            row.order = order;
            $scope.module.questions[row.id] = row;
          }
        });
        order++;
      });
      ////console.log($scope.module.questions);
      $scope.save();
      if (cb !== undefined) {
        cb();
      }
    };
    $scope.sortOptions = {
      update: function() {
        $scope.resetOrder(function() {
          alertify.success('Order saved');
        });
      },
    };
    $scope.module = module;
    $scope.setRows(module.questions);
    ////console.log(module);
    $scope.deleteQuestion = function(id, title) {
      alertify.confirm('Are you sure you want to delete "' + title + '"?', function(result) {
        if (result) {
          ////console.log('Delete ' + id);
          Questions.remove(id, function() {
            $scope.$apply(function() {
              _.each($scope.module.questions, function(question, questionId) {
                if (question.id === id) {
                  delete $scope.module.questions[questionId];
                  $scope.setRows($scope.module.questions, function() {
                    $scope.resetOrder();
                  });
                }
              });
              $scope.save();
            });
          });
        }
        else {
        }
        ////console.log('Delete ' + id);
      });
    };
    $scope.toggleQuestionStatus = function(id) {
      ////console.log(id);
      ////console.log($scope.module);
      _.each($scope.module.questions, function(question) {
        if (question.id == id) {
          if (question.published === 'false' || question.published == undefined) {
            question.published = 'true';
          }
          else {
            question.published = 'false';
          }
        }
      });
      $scope.save();
    };
    $scope.save = function() {
      Modules.set($scope.module.firebaseId, angular.copy($scope.module), function() {
        alertify.success('Saved');
      });
    };
    $scope.addQuestion = function () {
      alertify.prompt("What is the name of the new question?", function (e, str) {
        if (e) {
          Questions.add({
            title: str
          }, function(firebaseId) {
            $scope.$apply(function(){
              if ($scope.module.questions == undefined) {
                $scope.module.questions = {};
              }
              $scope.module.questions[firebaseId] = {
                data: {
                  title: str
                },
                id: firebaseId
              };
              $scope.setRows($scope.module.questions, function() {
                $scope.resetOrder(function() {
                  $scope.save();
                });
              });
            });
          });
        }
        else {
        }
      }, "A Questions");
    };
    $scope.fixObj = function(obj) {
      var temp = _.toArray(obj);
      return temp;
    };
  });
