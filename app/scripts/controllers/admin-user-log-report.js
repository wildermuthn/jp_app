'use strict';

angular.module('jpAppApp')
  .controller('AdminUserLogReportCtrl', function ($scope, user, Trainings) {
    $scope.user = user;
    ////console.log(user);
    Trainings.index(function(trainings) {
      ////console.log(trainings);
      _.each(user.modulesCompleted, function(module, moduleId) {
        ////console.log(module);
        _.each(trainings, function(training, trainingId) {
          _.each(training.modules, function(trainingModule, trainingModuleId) {
            if (moduleId === trainingModule.id) {
              ////console.log('found a training match: ' + training.title);
              $scope.$apply(function(){
                user.modulesCompleted[moduleId]['name'] = training.title + ': ' + user.modulesCompleted[moduleId]['name'];
              });
            }
          });
        });
      });
    });
    ////console.log(user);
  });
