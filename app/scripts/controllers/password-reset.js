'use strict';

angular.module('jpAppApp')
  .controller('PasswordResetCtrl', function ($scope, Users) {
    $scope.form = {};
    $scope.submit = function() {
      ////console.log($scope);
      var userId = $scope.form.userId;
     ////console.log('submitting:' + userId);
      Users.resetPassword(userId, function(response) {
        if (response) {
          alertify.success('A new password has been sent to ' + userId);
        }
        else {
          alertify.error('Something has gone wrong. Please try again or contact an administrator');
        }
      });
    };
  });
